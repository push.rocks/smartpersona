// @tsclass scope
import * as tsclass from '@tsclass/tsclass';

export {
  tsclass
};

// @pushrocks scope
import * as smartjson from '@pushrocks/smartjson';

export {
  smartjson
};
