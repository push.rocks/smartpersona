import * as plugins from './smartpersona.plugins';

export class Person extends plugins.smartjson.Smartjson implements plugins.tsclass.IPerson {
  public name: string;
  public sex: 'male' | 'female';
  public surname: string; 
  public title: string;
}
